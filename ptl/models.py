from datetime import datetime
import re

from django.utils.translation import gettext as _
from pydantic import (
    BaseModel,
    Field,
    IPvAnyAddress,
    ValidationError,
    field_validator
)
from typing_extensions import Annotated

from logs.models import RequestMethod


class LogData(BaseModel):
    """
    Log entry dataclass and validator.

    Represents a single log entry with detailed information
    about a request and response.
    """

    time: datetime
    remote_ip: IPvAnyAddress
    request_method: str
    endpoint: str
    response: Annotated[
        int,
        Field(strict=True, ge=100, le=599)
    ]
    bytes: Annotated[
        int,
        Field(strict=True, ge=0)
    ]

    def __init__(self, **data) -> None:
        request = data.pop("request")

        request_method, endpoint = self.parse_request(request)

        data["request_method"] = request_method
        data["endpoint"] = endpoint

        super().__init__(**data)

    @classmethod
    def parse_request(cls, request: str) -> list:
        try:
            return request.split()[:2]
        except (ValueError, IndexError, TypeError):
            raise ValidationError()

    @field_validator("time", mode="before")
    def parse_time(cls, value: str) -> datetime:
        try:
            return datetime.strptime(value, "%d/%b/%Y:%H:%M:%S %z")
        except ValueError:
            raise ValidationError(_("Invalid datetime format"))

    @field_validator("endpoint")
    def validate_endpoint(cls, value: str) -> str:
        uri_pattern = re.compile(r"^\/(?:[^\/]+\/?)*(?:\?[^#]*)?$")
        if not uri_pattern.match(value):
            raise ValidationError(_("Invalid URI format"))
        return value

    @field_validator("request_method")
    def validate_request_method(cls, value: str) -> str:
        if value not in [
            method for method, _ in RequestMethod.choices
        ]:
            raise ValidationError(_("Invalid request method"))
        return value
