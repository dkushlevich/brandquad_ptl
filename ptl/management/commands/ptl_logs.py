import ipaddress
import re

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils.translation import gettext as _
import requests

from ptl.base import PTL


class Command(BaseCommand):
    """
    Django management command to parse, transform and load
    log data from a given file URL.
    """

    help = _(
        "Parses, transforms, and loads "
        "log data from a specified file URL and server IP."
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "url",
            type=str,
        )
        parser.add_argument(
            "server_ip",
            type=str,
        )

    def handle(self, *args, **options):
        url = options.get("url")
        server_ip = options.get("server_ip")

        if not self._validate_url(url):
            self.stdout.write(
                self.style.ERROR(
                    ("Incorrect url")
                )
            )
            return

        if not self._validate_ip(server_ip):
            self.stdout.write(
                self.style.ERROR(
                    _("Incorrect server ip")
                )
            )
            return

        try:
            with transaction.atomic():
                ptl = PTL(url, server_ip)
                ptl.start_process()

                success_message = _(
                    "Data loading completed\n"
                    "Processed records: {counter}, "
                    "Saved to database: {success_counter}."
                ).format(
                    counter=ptl.counter,
                    success_counter=ptl.success_counter
                )

                self.stdout.write(self.style.SUCCESS(success_message))

        except requests.RequestException:
            self.stdout.write(
                self.style.ERROR(
                    _("Internet connection problem")
                )
            )
            return

    def _validate_url(self, url: str):
        return re.fullmatch(settings.REGEX_VALID_URL, url)

    def _validate_ip(self, ip: str):
        try:
            ipaddress.ip_address(ip)
            return True
        except ValueError:
            return False
