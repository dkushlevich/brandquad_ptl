import json
import logging
from typing import Generator, Optional

from django.db import DatabaseError
import requests
from pydantic import ValidationError

from logs.models import (
    Client,
    Endpoint,
    Log,
    Server
)
from ptl.models import LogData


logger = logging.getLogger(__name__)


class PTL:
    """
    Parse Transform Load (PTL) logs data class.

    :param url: The URL to fetch the logs data.
    :type url: str
    :param server_ip: The IP address of the logs server.
    :type server_ip: str
    """

    def __init__(self, url: str, server_ip: str):
        self.url = url
        self.server, _ = Server.objects.get_or_create(
            ip=server_ip
        )
        self.counter, self.success_counter = 0, 0

    def _parse_data(self) -> Generator[bytes, None, None]:
        """
        Parse the data from the URL.

        :raises HTTPError: If the response status is error.

        :rtype: Generator[bytes, None, None]
        :return: Lines of logs data from the response.
        """
        with requests.get(self.url, stream=True) as response:
            response.raise_for_status()
            yield from (line for line in response.iter_lines() if line)

    def _transform_data(self, log_data: bytes) -> Optional[LogData]:
        """
        Transform the parsed logs data and save it to dataclass.

        :param log_data: The raw line of log data.
        :type log_data: bytes

        :rtype: Optional[LogData]
        :return: The validated LogData instance or None if validation fails.
        """
        try:
            json_data = json.loads(log_data.decode("utf-8"))
            return LogData.model_validate(json_data)
        except (
            ValidationError,
            ValueError,
            TypeError,
            UnicodeDecodeError,
            json.JSONDecodeError
        ) as error:
            logger.error(
                f"Data validation error during transformation: {error}"
            )
        except Exception as error:
            logger.error(
                f"Transformation data error: {error}"
            )

    def _load_data(self, log_data: LogData) -> Log:
        """
        Load the transformed log data into the database.

        :param log_data: The transformed log data.
        :type log_data: LogData

        :rtype: Log
        """
        try:
            client, _ = Client.objects.get_or_create(
                ip=str(log_data.remote_ip)
            )
            endpoint, _ = Endpoint.objects.get_or_create(
                uri=log_data.endpoint
            )

            log = Log.objects.create(
                server=self.server,
                client=client,
                endpoint=endpoint,
                created_at=log_data.time,
                request_method=log_data.request_method,
                response_status_code=log_data.response,
                response_size=log_data.bytes,
            )
            self.success_counter += 1
            logger.info(
                f"Processed records: {self.counter}, "
                f"Saved to database: {self.success_counter}."
            )
            return log

        except DatabaseError as error:
            logger.error(
                f"Database error while loading data: {error}"
            )
        except Exception as error:
            logger.error(
                f"Error while loading data: {error}"
            )

    def start_process(self) -> None:
        """
        Start the PTL process.
        """
        for log in self._parse_data():
            self.counter += 1
            transformed_log = self._transform_data(log)
            if transformed_log:
                self._load_data(transformed_log)
