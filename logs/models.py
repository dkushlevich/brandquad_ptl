from django.db import models
from django.utils.translation import gettext_lazy as _


class IpModelMixin(models.Model):
    ip = models.GenericIPAddressField(
        _("IP"),
        unique=True,
        db_index=True
    )

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return self.ip


class Server(IpModelMixin):

    class Meta:
        verbose_name = _("Server")
        verbose_name_plural = _("Servers")


class Client(IpModelMixin):

    class Meta:
        verbose_name = _("Client")
        verbose_name_plural = _("Clients")


class Endpoint(models.Model):
    uri = models.CharField(
        _("URI")
    )

    class Meta:
        verbose_name = _("Endpoint")
        verbose_name_plural = _("Endpoints")
        ordering = ("uri", )

    def __str__(self) -> str:
        return self.uri


class RequestMethod(models.TextChoices):
    GET = "GET", "GET"
    POST = "POST", "POST"
    PUT = "PUT", "PUT"
    DELETE = "DELETE", "DELETE"
    PATCH = "PATCH", "PATCH"
    HEAD = "HEAD", "HEAD"
    OPTIONS = "OPTIONS", "OPTIONS"
    CONNECT = "CONNECT", "CONNECT"
    TRACE = "TRACE", "TRACE"


class Log(models.Model):
    server = models.ForeignKey(
        Server,
        on_delete=models.CASCADE,
        related_name="logs",
        verbose_name=_("Server")
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name="logs",
        verbose_name=_("Client")
    )
    endpoint = models.ForeignKey(
        Endpoint,
        on_delete=models.CASCADE,
        related_name="logs",
        verbose_name=_("Endpoint")
    )
    created_at = models.DateTimeField(
        _("Time")
    )
    request_method = models.CharField(
        _("Method"),
        choices=RequestMethod.choices,
        max_length=max(
            len(method) for method, _ in RequestMethod.choices
        ),
        db_index=True
    )
    response_status_code = models.PositiveSmallIntegerField(
        _("Reponse code"),
        db_index=True
    )
    response_size = models.PositiveIntegerField(
        _("Response size"),
    )

    class Meta:
        verbose_name = _("Log")
        verbose_name_plural = _("Logs")
        ordering = ("-created_at", )
