from django.contrib import admin
from django.contrib.auth.models import User, Group

from logs.models import (
    Endpoint,
    Log,
    Client,
    Server,
)


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ("id", "ip")
    search_fields = ("ip", )


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ("id", "ip")
    search_fields = ("ip", )
    list_filter = ("logs__server",)


@admin.register(Endpoint)
class EnpointAdmin(admin.ModelAdmin):
    list_display = ("id", "uri")
    search_fields = ("uri", )
    list_filter = ("logs__server", )


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "created_at",
        "client_ip",
        "endpoint_uri",
        "request_method",
        "response_status_code",
        "response_size",
    )
    search_fields = (
        "client__ip",
    )
    list_filter = (
        "server",
        "endpoint",
        "request_method",
        "response_status_code",
    )

    def get_queryset(self, request):
        return super().get_queryset(request).select_related(
            "client",
            "endpoint",
            "server"
        )

    def client_ip(self, obj):
        return obj.client.ip

    def endpoint_uri(self, obj):
        return obj.endpoint.uri

    client_ip.short_description = "IP адрес клиента"
    endpoint_uri.short_description = "URI"


admin.site.unregister(User)
admin.site.unregister(Group)
