from django.db.models import (
    Avg,
    Count,
    ExpressionWrapper,
    IntegerField,
    Q,
)
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import (
    extend_schema,
    extend_schema_view,
)
from rest_framework import filters
from rest_framework.viewsets import ReadOnlyModelViewSet

from logs.api.v1.serializers import (
    ClientStatsSerializer,
    EndpointStatsSerializer,
    LogSerializer,
    ServerSerilaizer,
)
from logs.api.v1.filters import (
    ClientFilter,
    EndpointFilter,
    LogFilter,
    ServerFilter,
)
from logs.models import (
    Endpoint,
    Log,
    Server,
    Client,
)


class StatsAnnotationMixin:
    """
    Mixin to create common stats annotations for several viewsets.
    """

    def get_stats_annotation(self) -> dict:
        return {
            "total_requests": Count("logs"),
            "success_requests_count": Count(
                "logs", filter=Q(logs__response_status_code__range=(200, 299))
            ),
            "redirect_requests_count": Count(
                "logs", filter=Q(logs__response_status_code__range=(300, 399))
            ),
            "error_requests_count": Count(
                "logs", filter=Q(logs__response_status_code__range=(400, 499))
            ),
            "server_errors_requests_count": Count(
                "logs", filter=Q(logs__response_status_code__range=(500, 599))
            ),
            "avg_response_size": ExpressionWrapper(
                Avg("logs__response_size"),
                output_field=IntegerField()
            )
        }


@extend_schema_view(
    retrieve=extend_schema(
        summary="Server Retrieve",
        tags=["Server"],
    ),
    list=extend_schema(
        summary="Servers List",
        tags=["Server"],
    ),
)
class ServerViewSet(
    StatsAnnotationMixin,
    ReadOnlyModelViewSet
):
    serializer_class = ServerSerilaizer
    filter_backends = (
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter
    )
    search_fields = (
        "ip",
    )
    filterset_class = ServerFilter

    def get_queryset(self):
        return Server.objects.annotate(
            endpoints_count=Count("logs__endpoint", distinct=True),
            clients_count=Count("logs__client", distinct=True),
            **self.get_stats_annotation()
        ).prefetch_related("logs__client", "logs__endpoint")


@extend_schema_view(
    retrieve=extend_schema(
        summary="Endpoint Retrieve",
        tags=["Endpoints"],
    ),
    list=extend_schema(
        summary="Endpoints List",
        tags=["Endpoints"],
    ),
)
class EndpointViewSet(
    StatsAnnotationMixin,
    ReadOnlyModelViewSet
):
    serializer_class = EndpointStatsSerializer
    filter_backends = (
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter
    )
    search_fields = (
        "uri",
    )
    filterset_class = EndpointFilter

    def get_queryset(self):
        return Endpoint.objects.filter(
            logs__server_id=self.kwargs.get("server_id")
        ).annotate(
            clients_count=Count("logs__client", distinct=True),
            **self.get_stats_annotation()
        )


@extend_schema_view(
    retrieve=extend_schema(
        summary="Client Retrieve",
        tags=["Clients"],
    ),
    list=extend_schema(
        summary="Clients List",
        tags=["Clients"],
    ),
)
class ClientViewSet(
    StatsAnnotationMixin,
    ReadOnlyModelViewSet
):
    serializer_class = ClientStatsSerializer
    filter_backends = (
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter
    )
    search_fields = (
        "uri",
    )
    filterset_class = ClientFilter

    def get_queryset(self):
        return Client.objects.filter(
            logs__server_id=self.kwargs.get("server_id")
        ).annotate(
            endpoints_count=Count("logs__endpoint", distinct=True),
            **self.get_stats_annotation()
        )


@extend_schema_view(
    retrieve=extend_schema(
        summary="Log Retrieve",
        tags=["Logs"],
    ),
    list=extend_schema(
        summary="Logs List",
        tags=["Logs"],
    ),
)
class LogViewSet(ReadOnlyModelViewSet):
    serializer_class = LogSerializer
    filter_backends = (
        DjangoFilterBackend,
        filters.OrderingFilter
    )
    filterset_class = LogFilter

    def get_queryset(self):
        return Log.objects.select_related(
            "client",
            "endpoint"
        ).filter(server_id=self.kwargs.get("server_id"))
