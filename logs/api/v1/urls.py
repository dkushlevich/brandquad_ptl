from django.urls import include, path
from rest_framework.routers import DefaultRouter

from logs.api.v1 import views

router = DefaultRouter()

router.register(
    "servers",
    views.ServerViewSet,
    basename="servers"
)


router.register(
    r"servers/(?P<server_id>\d+)/endpoints",
    views.EndpointViewSet,
    basename="server-endpoints"
)

router.register(
    r"servers/(?P<server_id>\d+)/clients",
    views.ClientViewSet,
    basename="server-clients"
)

router.register(
    r"servers/(?P<server_id>\d+)/logs",
    views.LogViewSet,
    basename="server-logs"
)


urlpatterns = [
    path("", include(router.urls))
]
