from rest_framework import serializers

from logs.models import (
    Client,
    Endpoint,
    Log,
    Server,
)


class StatsSerializerMixin:

    def get_fields(self) -> dict:
        fields = super().get_fields()
        fields.update(
            {
                "total_requests": serializers.IntegerField(),
                "success_requests_count": serializers.IntegerField(),
                "redirect_requests_count": serializers.IntegerField(),
                "error_requests_count": serializers.IntegerField(),
                "server_errors_requests_count": serializers.IntegerField(),
                "avg_response_size": serializers.IntegerField(),
            }
        )
        return fields


class EndpointSerializer(serializers.ModelSerializer):

    class Meta:
        model = Endpoint
        fields = (
            "id",
            "uri"
        )


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = (
            "id",
            "ip"
        )


# ServerViewSet


class ServerSerilaizer(
    StatsSerializerMixin,
    serializers.ModelSerializer
):
    endpoints_count = serializers.IntegerField()
    clients_count = serializers.IntegerField()

    class Meta:
        model = Server
        fields = (
            "id",
            "ip",
            "endpoints_count",
            "clients_count"
        )


# EndpointViewSet


class EndpointStatsSerializer(
    StatsSerializerMixin,
    EndpointSerializer
):
    clients_count = serializers.IntegerField()

    class Meta:
        model = Endpoint
        fields = (
            EndpointSerializer.Meta.fields + ("clients_count", )
        )


# ClientViewSet


class ClientStatsSerializer(
    StatsSerializerMixin,
    ClientSerializer
):
    endpoints_count = serializers.IntegerField()

    class Meta:
        model = Client
        fields = (
            ClientSerializer.Meta.fields + ("endpoints_count", )
        )


# LogViewSet


class LogSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    endpoint = EndpointSerializer()

    class Meta:
        model = Log
        fields = (
            "id",
            "client",
            "endpoint",
            "created_at",
            "request_method",
            "response_status_code",
            "response_size"
        )
