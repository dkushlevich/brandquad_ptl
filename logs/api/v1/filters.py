from django_filters import rest_framework as filters

from logs.models import (
    Client,
    Endpoint,
    Log,
    RequestMethod,
    Server
)


class StatsFilter(filters.FilterSet):
    total = filters.RangeFilter(
        field_name="total_requests"
    )
    success = filters.RangeFilter(
        field_name="success_requests_count"
    )
    redirects = filters.RangeFilter(
        field_name="redirect_requests_count"
    )
    errors = filters.RangeFilter(
        field_name="redirect_requests_count"
    )
    server_errors = filters.RangeFilter(
        field_name="server_errors_requests_count"
    )
    avg_size = filters.RangeFilter(
        field_name="avg_response_size"
    )

    class Meta:
        abstract = True


class ServerFilter(StatsFilter):
    endpoints_count = filters.RangeFilter()
    clients_count = filters.RangeFilter()

    class Meta:
        model = Server
        fields = ()


class ClientFilter(StatsFilter):
    endpoints_count = filters.RangeFilter()

    class Meta:
        model = Client
        fields = ()


class EndpointFilter(StatsFilter):
    clients_count = filters.RangeFilter()

    class Meta:
        model = Endpoint
        fields = ()


class LogFilter(filters.FilterSet):
    endpoint = filters.Filter(
        field_name="endpoint__uri",
        lookup_expr="icontains"
    )
    client = filters.Filter(
        field_name="client__ip",
        lookup_expr="icontains"
    )
    code = filters.Filter(
        field_name="response_status_code"
    )
    method = filters.ChoiceFilter(
        field_name="request_method",
        choices=RequestMethod.choices
    )
    size = filters.RangeFilter(
        field_name="response_size"
    )
    date = filters.DateFilter(
        field_name="created_at__date",
    )
    datetime = filters.DateTimeFromToRangeFilter(
        field_name="created_at",
    )

    class Meta:
        model = Log
        fields = (
            "code",
            "method",
            "size",
            "date",
            "datetime",
            "endpoint",
            "client",
        )
