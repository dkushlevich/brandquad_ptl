import pytest

from ptl.base import PTL
from ptl.models import LogData


@pytest.fixture()
def ptl():
    url = "http://fake_url.com/"
    server_ip = "192.0.1.2"
    return PTL(url, server_ip)


@pytest.fixture()
def valid_log():
    return b'{"time": "17/May/2015:08:05:32 +0000", "remote_ip": "93.180.71.3", "remote_user": "-", "request": "GET /downloads/product_1 HTTP/1.1", "response": 200, "bytes": 0, "referrer": "-", "agent": "Debian APT-HTTP/1.3 (0.8.16~exp12ubuntu10.21)"}'


@pytest.fixture()
def invalid_time_log():
    return b'{"time": "17May2015:08:05:32 +0000", "remote_ip": "93.180.71.3", "remote_user": "-", "request": "GET /downloads/product_1 HTTP/1.1", "response": 404, "bytes": 0, "referrer": "-", "agent": "Debian APT-HTTP/1.3 (0.8.16~exp12ubuntu10.21)"}'


@pytest.fixture()
def invalid_response_code_log():
    return b'{"time": "17May2015:08:05:32 +0000", "remote_ip": "93.180.71.3", "remote_user": "-", "request": "GET /downloads/product_1 HTTP/1.1", "response": 800, "bytes": 0, "referrer": "-", "agent": "Debian APT-HTTP/1.3 (0.8.16~exp12ubuntu10.21)"}'


@pytest.fixture()
@pytest.mark.django_db
def invalid_request_method():
    return b'{"time": "17May2015:08:05:32 +0000", "remote_ip": "93.180.71.3", "remote_user": "-", "request": "Fail /downloads/product_1 HTTP/1.1", "response": 800, "bytes": 0, "referrer": "-", "agent": "Debian APT-HTTP/1.3 (0.8.16~exp12ubuntu10.21)"}'


@pytest.fixture()
@pytest.mark.django_db
def raw_log_data():
    return '{"time": "17/May/2015:08:05:32 +0000", "remote_ip": "93.180.71.3", "remote_user": "-", "request": "GET /downloads/product_1 HTTP/1.1", "response": 200, "bytes": 0, "referrer": "-", "agent": "Debian APT-HTTP/1.3 (0.8.16~exp12ubuntu10.21)"}'


@pytest.fixture()
@pytest.mark.django_db
def another_raw_log_data():
    return '{"time": "17/May/2015:08:05:32 +0000", "remote_ip": "93.180.71.3", "remote_user": "-", "request": "GET /downloads/product_1 HTTP/1.1", "response": 200, "bytes": 0, "referrer": "-", "agent": "Debian APT-HTTP/1.3 (0.8.16~exp12ubuntu10.21)"}'


@pytest.fixture
def validated_log_data():
    return LogData(
        remote_ip="93.180.71.3",
        request="GET /downloads/product_1 /downloads/product_1 HTTP/1.1",
        time="17/May/2015:08:05:32 +0000",
        response=200,
        bytes=0
    )
