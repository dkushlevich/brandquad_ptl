import pytest
import requests_mock

from logs.models import Log
from ptl.base import PTL
from ptl.models import LogData


@pytest.mark.django_db(transaction=True)
class TestPTL:

    def test_data_parcing(
        self,
        ptl: PTL,
        raw_log_data: str,
        another_raw_log_data: str,
    ):
        with requests_mock.Mocker() as mock:
            mock.get(ptl.url, text="")

            with pytest.raises(StopIteration):
                next(ptl._parse_data())

            mock.get(ptl.url, text=raw_log_data)

            assert (
                next(ptl._parse_data())
                == raw_log_data.encode("utf-8")
            ), (
                "Parsed log data should match the raw log data."
            )

            mock.get(
                ptl.url,
                text="\n".join([raw_log_data, another_raw_log_data])
            )

            lines = ptl._parse_data()
            assert (
                (
                    raw_log_data.encode("utf-8"),
                    another_raw_log_data.encode("utf-8"),
                )
                == (
                    next(lines),
                    next(lines)
                )
            ), (
                "Response should parsing logs by strings."
            )

    def test_transform_data_valid_log(
        self,
        ptl: PTL,
        valid_log: bytes,
    ):
        validated_log_data = ptl._transform_data(valid_log)
        assert isinstance(validated_log_data, LogData), (
            "Valid log should be transformed successfully"
        )

    def test_transform_data_invalid_time_log(
        self,
        ptl: PTL,
        invalid_time_log: bytes,
    ):
        validated_log_data = ptl._transform_data(invalid_time_log)
        assert not isinstance(validated_log_data, LogData), (
            "Log with invalid time format should return None"
        )

    def test_transform_data_invalid_response_code_log(
        self,
        ptl: PTL,
        invalid_response_code_log: bytes,
    ):
        validated_log_data = ptl._transform_data(
            invalid_response_code_log
        )
        assert not isinstance(validated_log_data, LogData), (
            "Log with invalid response code should return None"
        )

    def test_transform_data_invalid_method(
        self,
        ptl: PTL,
        invalid_response_code_log: bytes,
    ):
        validated_log_data = ptl._transform_data(
            invalid_response_code_log
        )
        assert not isinstance(validated_log_data, LogData), (
            "Log with invalid method should return None"
        )

    def test_load_valid_data(
        self,
        ptl: PTL,
        validated_log_data: LogData
    ):

        logs_count = Log.objects.count()

        log = ptl._load_data(
            validated_log_data
        )
        assert isinstance(log, Log), (

        )

        assert Log.objects.count() == logs_count + 1, (
            "Log didn't save after load data."
        )

        log_compare = {
            validated_log_data.bytes: log.response_size,
            validated_log_data.endpoint: log.endpoint.uri,
            str(validated_log_data.remote_ip): log.client.ip,
            validated_log_data.request_method: log.request_method,
            validated_log_data.response: log.response_status_code,
            validated_log_data.time: log.created_at
        }

        for initial_field, db_field in log_compare.items():
            assert initial_field == db_field, (
                f"Field mismatch: expected {initial_field}, got {db_field}."
            )
