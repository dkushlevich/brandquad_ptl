<div align=center>
  
  # Brandquad logs PTL <br> (Реализация тестового задания) 
  <!-- <br> [Деплой](https://brandquadptl.ddns.net/api/v1/schema/swagger/) -->
  
  ![Brandquad_PTL_CI/CD](https://gitlab.com/dkushlevich/brandquad_ptl/badges/main/pipeline.svg)
  
  ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
  ![DjangoREST](https://img.shields.io/badge/DJANGO-REST-ff1709?style=for-the-badge&logo=django&logoColor=white&color=ff1709&labelColor=gray)
  ![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)

  ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
  ![Nginx](https://img.shields.io/badge/nginx-%23009639.svg?style=for-the-badge&logo=nginx&logoColor=white)
  ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)


</div>


## Описание тестового задания


### Задание:
Разработать Django приложение для обработки и агрегации Nginx лога.


#### Разработать Management command, которая:

- Принимает ссылку на файл с логами определённого формата

- Парсит логи и загружает их в базу данных

#### Продумать и реализовать структуру БД, которая:

- Эффективно хранит распарсенные данные из лога

- Содержит информацию как минимум об IP адресе клиента, дате из лога (размере ответа), HTTP-методе, URI запроса, коде и размере ответа

#### Отобразить загруженные данные в админ-панели Django и через API

#### Опционально:

- Хорошее оформление и комментирование кода (не излишнее, но хорошее)

- Упаковка проекта в docker/docker-compose

- Тесты

- Реализация в Django admin фильтров и поиска

- Реализация в DRF пагинации, фильтров и поиска, а также Swagger для описания.

#### Ограничения на стек технологий:

- Python

- Django, DRF


## Описание реализации тестового задания

Brandquad logs PTL - проект для сбора, хранения и отображения минимальной статистики Nginx логов с разных серверов, способный построчно обрабатывать файлы с большим объёмом информации. 

## Структура базы данных

<div align=center>
  <img src="https://prosport-dev.hammer.systems/media/events/5136/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2024-05-26_%D0%B2_1.12.24AM.png" alt="Database Structure" width="500">
</div>
<br>

## Основные ресурсы

**Сервер** - Немного расширил функционал, сделав проект хранителем логов с множества серверов с возможностью получить статистику по каждому из них. Для эффективного поиска добавлен индекс на поле ip.

**Лог** - Основная таблица проекта, в которую заносятся распарсенные данные

**Пользователь** - Для оптимизации БД вынес в отдельную таблицу, добавил ограничение на уникальность и индекс на ip.

**Эндпоинт** - Аналогично пользователю

## Запуск проекта

Проект контейнеризован с помощью Docker и Docker Compose, чтобы запустить проект выполнить следующие пункты:

1. Клонировать репозиторий.
   
2. Создать файл .env по подобию .env.example
  
3. Запустить проект с помощью команды:
  ```bash
    docker compose -f docker-compose-local.yaml up --build -d
  ```

4. Создать суперпользователя django:
  ```bash
    docker exec -it ptl_api python manage.py createsuperuser
  ```


## Парсинг, обработка и сохранение логов в БД

Процесс реализован с помощью Management Command с двумя обязательными параметрами - url(ссылка на файл с логам) и server_ip (ip сервера, которому принадлежат логи).
  ```bash
    docker exec -it ptl_api python manage.py ptl_logs <url> <server_ip>
  ```

После первичной валидации вызывается класс, который построчно парсит, подготавливает к записи и записывает в БД логи.

> Атомарность транзакции обеспечивается только при обрыве соеденения, при ошибках валидации/записи отдельного лога скрипт продолжает работу


## API

Для каждого ресурса реализованы GET запросы, в которых наряду с основными полями также отображаются агрегированные данные. Реализованы пагинация, фильтрация и поиск.

>Подробная документация API доступна по [ссылке](https://brandquadptl.ddns.net/api/v1/schema/swagger/)

## Тесты

С помощью pytest протестирован класс сбора и записи логов.

Запуск тестов:

```bash
  docker exec -it ptl_api pytest --disable-warnings
```

Если бы не косые взгляды девушки на мой компьютер, протестировал бы и API :) На всякий случай оставляю [ссылку](https://github.com/dkushlevich/foodgram-project-react/tree/master/backend/tests)  на 100% coverage другого проекта.

## Деплой

UPD: Изначально проект был задеплоен на VPS (можно посмотреть историю пайплайнов), на данный момент сервер отключен

<!-- Для удобства проверки API и админ-панели проект развёрнут на VPS (в корне проекта лежит постман коллекция).

Админ панель: https://brandquadptl.ddns.net/admin/

Логин: admin
Пароль: admin -->

## CI/CD
Частично реализован CI/CD, проект автоматически деплоится и развёртывается на VPS с помощью Gitlab Runner-а. Сертификаты лежат в docker volume, все запросы обрабатываются через nginx в контейнере (внешний nginx на сервере отсутствует).

</details>

<div align=center>

## Контакты

[![Telegram Badge](https://img.shields.io/badge/-dkushlevich-blue?style=social&logo=telegram&link=https://t.me/dkushlevich)](https://t.me/dkushlevich) [![Gmail Badge](https://img.shields.io/badge/-dkushlevich@gmail.com-c14438?style=flat&logo=Gmail&logoColor=white&link=mailto:dkushlevich@gmail.com)](mailto:dkushlevich@gmail.com)

</div>
