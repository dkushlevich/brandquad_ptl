from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularSwaggerView,
    SpectacularRedocView,
)


urlpatterns = [
    # Admin
    path("admin/", admin.site.urls),

    # Docs
    path("api/v1/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/v1/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"), name="redoc"
    ),
    path(
        "api/v1/schema/swagger/",
        SpectacularSwaggerView.as_view(url_name="schema"), name="swagger"
    ),

    # logs app
    path("api/v1/", include("logs.api.v1.urls")),
]

if settings.DEBUG:
    urlpatterns += [
        # Database monitoring
        path("api/silk/", include("silk.urls", namespace="silk"))
    ]
