#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset

export DJANGO_SETTINGS_MODULE=brandquad_ptl.settings

python manage.py migrate --noinput

python manage.py collectstatic --noinput

gunicorn --workers 4 --worker-class uvicorn.workers.UvicornWorker --timeout 120 --bind 0.0.0.0:8000 brandquad_ptl.asgi:application
